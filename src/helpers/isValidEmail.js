import { validators } from "../utils/validators";
const validator = new RegExp(validators);

export const isValidEmail = (email) => validator.test(email);
