export const createElement = ({
  tag,
  className,
  attribute,
  placeholder,
  type,
  value = "",
  id,
  tabindex,
}) => {
  const element = document.createElement(tag);
  element.classList.add(className);
  element.setAttribute(attribute.label, attribute.value);
  element.placeholder = placeholder;
  element.type = type;
  if (id) {
    element.id = id;
  }
  element.value = value;
  return element;
};
