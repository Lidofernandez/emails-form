import { isValidEmail } from "./isValidEmail";

export const getEmailCount = (container) => {
  const count = Array.from(
    container.querySelectorAll("input")
  ).filter((child) => isValidEmail(child.value));
  return count.length;
};
