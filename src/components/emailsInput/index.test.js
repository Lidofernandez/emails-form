import { screen, fireEvent } from "@testing-library/dom";

import { EmailsInput } from "./index";

describe("EmailsInput", () => {
  let getRootElement;
  beforeEach(() => {
    getRootElement = () => document.querySelector("div");
    const divElement = document.createElement("div");
    document.body.appendChild(divElement);
  });

  afterEach(() => {
    document.body.innerHTML = "";
  });
  it("should render", () => {
    new EmailsInput(getRootElement(), { placeholder: "hello there" });

    screen.getByPlaceholderText("hello there");
    screen.getByTestId("emails-input");
  });

  it("should focus on the input", () => {
    new EmailsInput(getRootElement());
    const element = screen.getByTestId("email-input");
    expect(document.activeElement).toBe(element);
  });

  it("should render an input field on pressing Enter or Comma", () => {
    new EmailsInput(getRootElement());
    const element = screen.getByTestId("emails-input");
    fireEvent.keyDown(element, { code: "Enter" });
    fireEvent.keyDown(element, { code: "Comma" });
    const emailInputs = screen.getAllByPlaceholderText("Add more people...");
    expect(emailInputs.length).toBe(3);
  });

  it("should return email count", () => {
    const emails = new EmailsInput(getRootElement());
    emails.addEmail({ value: "test1m@email.com" });
    emails.addEmail({ value: "test2@email.com" });
    emails.addEmail({ value: "test3" });

    expect(emails.getEmailCount()).toBe(2);
  });

  it("should return all emails", () => {
    const emails = new EmailsInput(getRootElement());
    emails.addEmail({ value: "test1m@email.com" });
    emails.addEmail({ value: "test2@email.com" });
    emails.addEmail({ value: "test3" });

    expect(emails.getAllEmails()).toEqual([
      { email: "test1m@email.com", isValid: true },
      { email: "test2@email.com", isValid: true },
      { email: "test3", isValid: false },
    ]);
  });

  it("should replace all emails", () => {
    const emails = new EmailsInput(getRootElement());
    emails.addEmail({ value: "test1m@email.com" });
    emails.addEmail({ value: "test2@email.com" });
    emails.addEmail({ value: "test3" });
    emails.replaceAllEmails("test");
    screen.getAllByDisplayValue("test");
  });

  describe("with paste event handler", () => {
    it("should render an input field", () => {
      new EmailsInput(getRootElement());
      const element = screen.getByTestId("emails-input");
      fireEvent.paste(element, {
        clipboardData: {
          getData: () => "test1@gmail.com,test2@gmail.com",
        },
      });

      screen.getByDisplayValue("test1@gmail.com");
      screen.getByDisplayValue("test2@gmail.com");
    });

    it("should render valid emails", () => {
      new EmailsInput(getRootElement());
      const element = screen.getByTestId("emails-input");
      fireEvent.paste(element, {
        clipboardData: {
          getData: () => "not_valid_email,valid_email@gmail.com",
        },
      });

      screen.getByDisplayValue("valid_email@gmail.com");
      expect(screen.queryByDisplayValue("not_valid_email")).toBeNull();
    });

    it("should not render invalid emails", () => {
      new EmailsInput(getRootElement());
      const element = screen.getByTestId("emails-input");
      fireEvent.paste(element, {
        clipboardData: {
          getData: () => "",
        },
      });

      const emailInputs = screen.getAllByPlaceholderText("Add more people...");
      expect(emailInputs.length).toBe(1);
    });
  });
});
