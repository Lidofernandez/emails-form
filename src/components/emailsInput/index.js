import { addEmail } from "../../helpers/addEmail";
import { createElement } from "../../helpers/createElement";
import { getEmailCount } from "../../helpers/getEmailCount";
import { isValidEmail } from "../../helpers/isValidEmail";
import { InputField } from "../inputField";
import "./index.scss";

const optionsDefault = {
  placeholder: "Add more people...",
};
let id = 1;

const EmailsInput = function (rootElement, options) {
  this.options = {
    ...optionsDefault,
    ...options,
  };
  this.container = createElement({
    tag: "div",
    className: "emails-input",
    attribute: { label: "data-testid", value: "emails-input" },
    id: `new-inputs-field-${rootElement.id || id++}`,
  });
  this.rootElement = rootElement.appendChild(this.container);
  this.container.addEventListener("keydown", this.handleAddEmail.bind(this));
  this.container.addEventListener("paste", this.handleOnPaste.bind(this));
  this.init();
};

EmailsInput.prototype.addEmail = function (value) {
  addEmail(this.addInput(this.container, { ...this.options, ...value }));
};

EmailsInput.prototype.addInput = function (container, options) {
  this.inputField = new InputField(container, {
    placeholder: options.placeholder,
    value: options.value,
  });

  return this.inputField;
};

EmailsInput.prototype.getEmailCount = function () {
  return getEmailCount(this.container);
};

EmailsInput.prototype.handleAddEmail = function (event) {
  if (event.code === "Enter" || event.code === "Comma") {
    return this.addEmail();
  }
};

EmailsInput.prototype.handleOnPaste = function (event) {
  event.preventDefault();
  // According to the MDN documentation this will always return a
  // string https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer/getData
  const emails = event.clipboardData.getData("text/plain");
  if (!emails) {
    return;
  }

  emails.split(",").forEach((email) => {
    if (isValidEmail(email)) {
      this.addEmail({ value: email });
    }
  });
};

EmailsInput.prototype.init = function () {
  return addEmail(this.addInput(this.rootElement, this.options));
};

EmailsInput.prototype.getAllEmails = function () {
  const emails = [];
  Array.from(this.container.querySelectorAll("input")).forEach((input) => {
    if (input.value) {
      emails.push({ email: input.value, isValid: isValidEmail(input.value) });
    }
  });
  return emails;
};

EmailsInput.prototype.replaceAllEmails = function (value) {
  Array.from(this.container.querySelectorAll("input")).forEach((input) => {
    if (input.value) {
      input.value = value;
    }
  });
};

export { EmailsInput };
