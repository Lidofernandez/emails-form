import { fireEvent, screen } from "@testing-library/dom";
import { Icon } from "./index";

describe("Icon", () => {
  let container;

  beforeEach(() => {
    container = () => document.querySelector("div");
    const divElement = document.createElement("div");
    document.body.appendChild(divElement);
  });
  afterEach(() => {
    document.body.innerHTML = "";
  });
  it("should render", () => {
    const icon = new Icon(container());
    icon.render();
    screen.getByTestId("icon");
    screen.getByText("x");
  });
  it("should render", () => {
    const handleOnClick = jest.fn();
    const icon = new Icon(container());
    icon.render();
    icon.handleOnClick(handleOnClick);
    fireEvent.click(screen.getByTestId("icon"));
    expect(handleOnClick).toHaveBeenCalled();
  });
});
