import { createElement } from "../../helpers/createElement";
import "./index.scss";

const Icon = function (container) {
  this.container = container;
  this.icon = createElement({
    tag: "span",
    className: "icon",
    attribute: { label: "data-testid", value: "icon" },
  });
  this.icon.innerHTML = "x";
};

Icon.prototype.render = function () {
  return this.container.appendChild(this.icon);
};

Icon.prototype.handleOnClick = function (handleOnClick) {
  this.icon.addEventListener("click", handleOnClick);
};

export { Icon };
