import { fireEvent, screen } from "@testing-library/dom";

import { InputField } from "./index";

describe("InputField", () => {
  let container;

  beforeEach(() => {
    container = () => document.querySelector("div");
    const divElement = document.createElement("div");
    document.body.appendChild(divElement);
  });
  afterEach(() => {
    document.body.innerHTML = "";
  });
  it("should render", () => {
    new InputField(container(), {
      placeholder: "testPlaceholder",
      value: "testValue",
    });

    screen.getByPlaceholderText("testPlaceholder");
    screen.getByDisplayValue("testValue");
  });

  it("should add valid class when email is valid", () => {
    new InputField(container(), {
      placeholder: "testPlaceholder",
      value: "valid@email.com",
    });
    screen.getByDisplayValue("valid@email.com");

    expect(screen.getByTestId("input-field-container").classList).toContain(
      "input-field--is-valid"
    );
  });

  it("should add error class when email is invalid", () => {
    new InputField(container(), {
      placeholder: "testPlaceholder",
      value: "invalidEmail",
    });
    screen.getByDisplayValue("invalidEmail");
    expect(screen.getByTestId("input-field-container").classList).toContain(
      "input-field--has-error"
    );
  });

  it("should add error class when email is invalidsss", () => {
    new InputField(container(), {
      placeholder: "testPlaceholder",
      value: "invalidEmail",
    });
    screen.getByDisplayValue("invalidEmail");
    expect(screen.getByTestId("input-field-container").classList).toContain(
      "input-field--has-error"
    );
  });

  it("should handle email validation on change", () => {
    new InputField(container(), {
      placeholder: "testPlaceholder",
    });
    const input = screen.getByTestId("email-input");
    fireEvent.change(input, { target: { value: "valid@email.com" } });
    screen.getByDisplayValue("valid@email.com");
    expect(screen.getByTestId("input-field-container").classList).toContain(
      "input-field--is-valid"
    );
  });

  it("should remove commas on keyup", () => {
    new InputField(container(), {
      placeholder: "testPlaceholder",
      value: ",",
    });
    const input = screen.getByTestId("email-input");

    fireEvent.keyUp(input, { key: "Enter" });
    expect(input.value).toBe("");
  });

  it("should remove element on click", () => {
    new InputField(container(), {
      placeholder: "testPlaceholder",
      value: "test@gmail.com",
    });
    screen.getByDisplayValue("test@gmail.com");
    const icon = screen.getByTestId("icon");
    fireEvent.click(icon);
    expect(screen.queryByDisplayValue("test@gmail.com")).toBeNull();
  });
});
