import { createElement } from "../../helpers/createElement";
import { isValidEmail } from "../../helpers/isValidEmail";
import { Icon } from "../icon";

import "./index.scss";

let id = 1;

const InputField = function (container, { placeholder, value } = {}) {
  this.container = container;
  this.placeholder = placeholder;
  this.value = value;
  this.inputFieldContainer = createElement({
    tag: "div",
    className: "input-field__container",
    attribute: { label: "data-testid", value: "input-field-container" },
  });
  this.input = createElement({
    tag: "input",
    type: "email",
    className: "input-field",
    attribute: { label: "data-testid", value: "email-input" },
    placeholder: this.placeholder,
    value: this.value,
    id: id++,
  });
  this.inputFieldContainer.appendChild(this.input);
  this.icon = new Icon(this.inputFieldContainer);
  this.icon.handleOnClick(this.handleOnRemove.bind(this));

  if (value) {
    this.setValidClassName(value);
  }

  this.input.addEventListener("change", this.handleOnChange.bind(this));
  this.input.addEventListener("keyup", this.handleOnKeyUp.bind(this));

  this.init();
};

InputField.prototype.setValidClassName = function (value) {
  if (isValidEmail(value)) {
    this.inputFieldContainer.classList.remove("input-field--has-error");
    this.inputFieldContainer.classList.add("input-field--is-valid");
    this.icon.render();
    return;
  }

  this.inputFieldContainer.classList.remove("input-field--is-valid");
  this.inputFieldContainer.classList.add("input-field--has-error");
};

InputField.prototype.removeValues = function (event) {
  this.input.value = event.currentTarget.value.replace(",", "");
};

InputField.prototype.handleOnChange = function (event) {
  this.setValidClassName(event.currentTarget.value);
  this.removeValues(event);
};

InputField.prototype.handleOnKeyUp = function (event) {
  this.removeValues(event);
};

InputField.prototype.init = function () {
  return this.container.appendChild(this.inputFieldContainer);
};

InputField.prototype.focus = function () {
  this.input.focus();
};

InputField.prototype.handleOnRemove = function () {
  this.inputFieldContainer.remove();
};

export { InputField };
