import { EmailsInput } from "./components/emailsInput";
import "./index.scss";

const inputContainerNode = document.querySelector("#emails-input");
const options = {};
const emails = new EmailsInput(inputContainerNode, options);

const addEmailButton = document.getElementById("add-email");
let randomEmail = 1;
addEmailButton.addEventListener("click", (event) => {
  emails.addEmail({ value: `random${randomEmail++}@email.com` });
});

const countValidEmails = document.getElementById("count-emails");
countValidEmails.addEventListener("click", () => {
  alert(emails.getEmailCount());
});
