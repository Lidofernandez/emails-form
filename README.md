**Goal**

This project implements the following API:

- A method to get all entered emails. Both valid and invalid.
- A method to replace all entered emails with new ones.
- Ability to subscribe for emails list changes.
- Ability to add an email
- Abilitty to count the emails

**How to run**

This project is build with simple wepack configuration, so you only need to run:

```
npm run start
```

**Coverage/test**

This project has more than 90% coverage, and it's build with jest, testing-library, and wallaby.js

**Folder structure**

simple components with a js, a css file, helpers and utils function, the components are:

- EmailsInput: component that generates a container and consumes InputField, handles events for adding new Inputs
- InputField: component that generates an input field, and consumes Icon component, handles validation, input removal and updates
- Icon: generates a span that it's styled as an icon, it's only UI and it's possible to pass events
